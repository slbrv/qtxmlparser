#ifndef QT_FICTION_BOOK_H
#define QT_FICTION_BOOK_H

#include <QtCore/QString>

#include "qt_xml_document.h"

class QFictionBookParseException : public std::exception
{
public:
    explicit QFictionBookParseException(const char* message);
    const char* what() const noexcept override;
private:
    const char* _message;
};

class QFictionBook
{
public:
    QFictionBook();
    QFictionBook(const QXMLDocument& document) noexcept(false);

    QString getAuthor() const;
    QString getName() const;
    QString getAnnotation() const;
private:
    QString _author;
    QString _name;
    QString _annotation;
};

#endif // QT_FICTION_BOOK_H
