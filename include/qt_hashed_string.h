#ifndef QT_HASHED_STRING_H
#define QT_HASHED_STRING_H

#include <QString>

class QHashedString
{
public:
    QHashedString();
    QHashedString(const QString&);
    QHashedString(const QHashedString&);

    qint64 getHash() const;
    QString getString() const;

    QHashedString& operator=(const QHashedString&);
    friend bool operator==(const QHashedString& lhs, const QHashedString& rhs);
    friend bool operator<(const QHashedString& lhs, const QHashedString& rhs);
    friend bool operator>(const QHashedString& lhs, const QHashedString& rhs);

    ~QHashedString();

private:
    QString _string;
    qint64 _hash;

    qint64 rehash(const QString&);
};

#endif // QT_HASHED_STRING_H
