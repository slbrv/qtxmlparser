#ifndef QT_XML_DOCUMENT_H
#define QT_XML_DOCUMENT_H

#include <QString>
#include <QSharedPointer>
#include <QMap>
#include <QVector>

#include <qt_hashed_string.h>

class QXMLElement
{
public:
    enum Type
    {
        DEFAULT,
        CLOSER,
        DECL
    };

    QXMLElement();
    QXMLElement(const QXMLElement&);
    QXMLElement(Type type, const QString& name);

    void setName(const QString& name);
    void setType(Type type);
    void setData(const QString& data);

    void addChild(QXMLElement* child);
    void addAttrib(
            const QString& key,
            const QString& value);

    Type getType() const;
    QString getName() const;
    QString& getData();
    QXMLElement* getChild(int index);
    QXMLElement* getParent();
    QVector<QXMLElement*> getChildren(const QString& name);
    QVector<QXMLElement*> getChildren();
    QString getAttrib(const QString& key);

    QVector<QXMLElement*> find(const QString& name);

    QXMLElement& operator=(const QXMLElement&) = default;

    ~QXMLElement();

private:
    Type _type;
    QHashedString _name;
    QXMLElement* _parent;
    QString _data;
    QVector<QXMLElement*> _children;
    QMap<QHashedString, QString> _attribs;
};

class QXMLDocumentParseException : public std::exception
{
public:
    explicit QXMLDocumentParseException(const char* message);
    const char* what() const noexcept override;
private:
    const char* _message;
};

class QXMLDocument
{
public:
    QXMLDocument();
    QXMLDocument(const QXMLDocument&);
    QXMLDocument(const QString& name, const QString& rawDoc) noexcept(false);
    QXMLDocument(
            QXMLDocument(* parse)(const QString&),
            const QString& rawDoc) noexcept(false);

    QXMLDocument& operator=(const QXMLDocument&);

    QVector<QXMLElement*> find(const QString& name) const;

    QXMLElement* getRoot();
    QString getName() const;

    ~QXMLDocument();
private:
    QString _name;
    QXMLElement* _root;

    void defaultParse(const QString& rawDoc) noexcept(false);
};

void recursiveSearch(QVector<QXMLElement*>& found, QXMLElement* root, const QString& name);

#endif // QT_XML_DOCUMENT_H
