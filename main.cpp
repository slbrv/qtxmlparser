#include <QCoreApplication>
#include <QTextStream>
#include <QtCore/QFile>

#include <iostream>

#include <data/res.h>

#include <qt_fiction_book.h>
#include <qt_xml_document.h>

QString getDataFilePath(const char* file)
{
    QString path(DATA_PATH);
    path += file;
    return path;
}

int main(int argc, char* argv[])
{
    // TEST 1

    QFile file1(getDataFilePath("fb2_1.fb2"));
    if (!file1.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        std::cout << "Failed to load file fb2_1.fb2" << std::endl;
        return 1;
    }

    QTextStream ts1(&file1);
    QString fb2Test1;
    while (!ts1.atEnd())
        fb2Test1 += ts1.readLine();
    file1.close();

    QFictionBook fb1(QXMLDocument("fb1", fb2Test1));
    std::cout << "Test book 1" << std::endl;
    std::cout << "Name: " << fb1.getName().toStdString() << std::endl;
    std::cout << "Author: " << fb1.getAuthor().toStdString() << std::endl;
    std::cout << "Annotation: " << fb1.getAnnotation().toStdString() << std::endl;
    std::cout << "=========================" << std::endl;

    // TEST 2

    QFile file2(getDataFilePath("fb2_2.fb2"));
    if (!file2.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        std::cout << "Failed to load file fb2_2.fb2" << std::endl;
        return 1;
    }

    QTextStream ts2(&file2);
    QString fb2Test2;
    while (!ts2.atEnd())
        fb2Test2 += ts2.readLine();
    file2.close();

    QFictionBook fb2(QXMLDocument("fb2", fb2Test2));
    std::cout << "Test book 2" << std::endl;
    std::cout << "Name: " << fb2.getName().toStdString() << std::endl;
    std::cout << "Author: " << fb2.getAuthor().toStdString() << std::endl;
    std::cout << "Annotation: " << fb2.getAnnotation().toStdString() << std::endl;
    std::cout << "=========================" << std::endl;
    return 0;
}
