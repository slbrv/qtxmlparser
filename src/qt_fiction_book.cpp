#include <qt_fiction_book.h>

QFictionBook::QFictionBook()
        :_author("null"),
         _name("null"),
         _annotation("null")
{

}

QFictionBook::QFictionBook(const QXMLDocument& document) noexcept(false)
        :QFictionBook()
{
    if (!document.find("QFictionBook").isEmpty())
    {
        QString msg = "Fiction book parse error: ";
        msg.push_back(document.getName());
        msg.push_back(" isn't a fiction book");
        throw QFictionBookParseException(msg.toStdString().c_str());
    }

    QVector<QXMLElement*> authorList = document.find("author");
    if (!authorList.isEmpty())
    {
        QXMLElement* authorElement = authorList.at(0);
        QString firstName, lastName;
        for (auto child : authorElement->getChildren())
        {
            if (child->getName() == "first-name")
                firstName = child->getData();
            if (child->getName() == "last-name")
                lastName = child->getData();
        }
        _author = firstName + " " + lastName;
    }

    QVector<QXMLElement*> bookNameList = document.find("book-title");
    if (!bookNameList.isEmpty())
    {
        QXMLElement* bookNameElement = bookNameList.at(0);
        _name = bookNameElement->getData();
    }

    QVector<QXMLElement*> annotationList = document.find("annotation");
    if (!annotationList.isEmpty())
    {
        QXMLElement* annotationElement = annotationList.at(0);
        QString annotation;
        for (auto p : annotationElement->getChildren())
        {
            if (p->getName() == "p")
            {
                annotation = p->getData() + "\n";
            }
        }
        _annotation = annotation;
    }
}
QString QFictionBook::getAuthor() const
{
    return _author;
}
QString QFictionBook::getName() const
{
    return _name;
}
QString QFictionBook::getAnnotation() const
{
    return _annotation;
}

QFictionBookParseException::QFictionBookParseException(const char* message)
        :_message(message)
{

}

const char* QFictionBookParseException::what() const noexcept
{
    return _message;
}