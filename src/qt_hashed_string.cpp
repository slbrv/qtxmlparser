#include <qt_hashed_string.h>

#include <QtCore/QDataStream>
#include <QtCore/qmath.h>

QHashedString::QHashedString()
        :_string(),
         _hash(0)
{

}

QHashedString::QHashedString(const QString& copy)
        :_string(copy),
         _hash(rehash(copy))
{

}

QHashedString::QHashedString(const QHashedString& copy)
        :_string(copy._string),
         _hash(copy._hash)
{

}

qint64 QHashedString::getHash() const
{
    return _hash;
}

QString QHashedString::getString() const
{
    return _string;
}

QHashedString& QHashedString::operator=(const QHashedString& copy)
{
    _string = copy._string;
    _hash = copy._hash;

    return *this;
}

QHashedString::~QHashedString()
{

}

qint64 QHashedString::rehash(const QString& string)
{
    qint64 hash = 0;
    for (int i = 0; i < string.length(); ++i)
    {
        hash += qPow(string.at(i).unicode() % 128 * 2, i % 8);
    }
    return hash;
}
bool operator==(const QHashedString& lhs, const QHashedString& rhs)
{
    return lhs._hash == rhs._hash;
}

bool operator<(const QHashedString& lhs, const QHashedString& rhs)
{
    return lhs._hash < rhs._hash;
}

bool operator>(const QHashedString& lhs, const QHashedString& rhs)
{
    return lhs._hash > rhs._hash;
}
