﻿#include <qt_xml_document.h>

#include <QtCore/QRegularExpressionMatchIterator>
//
// QXMLElement implementation
//

QXMLElement::QXMLElement()
        :_type(DEFAULT),
         _name(),
         _parent(nullptr),
         _data("null"),
         _children(),
         _attribs()
{

}

QXMLElement::QXMLElement(const QXMLElement& copy)
        :_type(DEFAULT),
         _name(copy._name),
         _parent(copy._parent),
         _data("null"),
         _children(copy._children),
         _attribs(copy._attribs)
{

}

QXMLElement::QXMLElement(
        const Type type,
        const QString& name)
        :_type(type),
         _name(name),
         _parent(nullptr),
         _data("null"),
         _children(),
         _attribs()
{

}

void QXMLElement::setName(const QString& name)
{
    _name = name;
}

void QXMLElement::addChild(QXMLElement* child)
{
    child->_parent = this;
    _children.push_back(child);
}

void QXMLElement::addAttrib(const QString& key, const QString& value)
{
    _attribs.insert(key, value);
}

QXMLElement::Type QXMLElement::getType() const
{
    return _type;
}

QString QXMLElement::getName() const
{
    return _name.getString();
}

QXMLElement* QXMLElement::getChild(int index)
{
    return _children.at(index);
}

QVector<QXMLElement*> QXMLElement::getChildren(const QString& name)
{
    QVector<QXMLElement*> found;
    QHashedString hashedName(name);
    for (auto item : _children)
    {
        if (item->_name == hashedName)
            found.push_back(item);
    }
    return found;
}

QVector<QXMLElement*> QXMLElement::getChildren()
{
    return _children;
}

QString QXMLElement::getAttrib(const QString& key)
{
    auto iter = _attribs.find(key);
    QString found = iter != _attribs.end() ? iter.value() : "null";
    return found;
}

QXMLElement::~QXMLElement()
{

}
QXMLElement* QXMLElement::getParent()
{
    return _parent;
}

void QXMLElement::setType(QXMLElement::Type type)
{
    _type = type;
}

void QXMLElement::setData(const QString& data)
{
    _data = data;
}

QString& QXMLElement::getData()
{
    return _data;
}

QVector<QXMLElement*> QXMLDocument::find(const QString& name) const
{
    QVector<QXMLElement*> result;
    recursiveSearch(result, _root, name);
    return result;
}

QVector<QXMLElement*> QXMLElement::find(const QString& name)
{
    QVector<QXMLElement*> result;
    recursiveSearch(result, this, name);
    return result;
}

//
// QXMLDocumentParseException implementation
//

QXMLDocumentParseException::QXMLDocumentParseException(const char* message)
        :
        _message(message)
{

}

const char* QXMLDocumentParseException::what() const noexcept
{
    return _message;
}

//
// QXMLDocument implementation
//

QXMLDocument::QXMLDocument()
        :_name(""),
         _root(nullptr)
{

}

QXMLDocument::QXMLDocument(const QXMLDocument& copy)
        :_name(copy._name),
         _root(copy._root)
{

}

QXMLDocument::QXMLDocument(
        const QString& name,
        const QString& rawDoc) noexcept(false)
        :_name(name),
         _root(nullptr)
{
    try
    {
        defaultParse(rawDoc);
    }
    catch (QXMLDocumentParseException& e)
    {
        throw e;
    }
}

QXMLDocument::QXMLDocument(
        QXMLDocument(* parse)(const QString&),
        const QString& rawDoc)
{
    try
    {
        parse(rawDoc);
    }
    catch (QXMLDocumentParseException& e)
    {
        throw e;
    }
}

QXMLDocument& QXMLDocument::operator=(const QXMLDocument& copy)
{
    return *this;
}

void recursiveSearch(
        QVector<QXMLElement*>& found,
        QXMLElement* root,
        const QString& name)
{
    for (auto item : root->getChildren())
        recursiveSearch(found, item, name);
    if (root->getName() == name)
        found.push_back(root);
}

QXMLElement* QXMLDocument::getRoot()
{
    return _root;
}

void QXMLDocument::defaultParse(const QString& rawDoc) noexcept(false)
{
    QRegularExpression tagsRegExp("(<[^>]*>)|([^><]*)", QRegularExpression::MultilineOption);
    QRegularExpressionMatchIterator tagsIterator = tagsRegExp.globalMatch(rawDoc);
    QStringList tags;

    QXMLElement* currentElement = nullptr;

    while (tagsIterator.hasNext())
    {
        QRegularExpressionMatch tagsMatch = tagsIterator.next();
        QString captured = tagsMatch.captured(0);
        if (!captured.isEmpty() && captured.at(0) != ' ') tags << captured;
    }
    if (tags.isEmpty())
        throw QXMLDocumentParseException("XML document parse error:"
                                         "empty document");
    for (auto item : tags)
    {
        if (item.at(0) == '<')
        {
            QRegularExpression attribsRegExp(R"([^<>\s=""]*)");
            QRegularExpressionMatchIterator attribsIterator =
                    attribsRegExp.globalMatch(item);
            QStringList tagAttribs;
            while (attribsIterator.hasNext())
            {
                QRegularExpressionMatch attribsMatch = attribsIterator.next();
                QStringList captured = attribsMatch.capturedTexts();
                for (auto attrib : captured)
                    if (!attrib.isEmpty())
                        tagAttribs << attrib;
            }
            if (!tagAttribs.empty())
            {
                QXMLElement element;
                auto name = tagAttribs.at(0);
                if (name.at(0) == '?')
                {
                    element.setType(QXMLElement::Type::DECL);
                    element.setName(name.mid(1, name.size() - 1));
                    if (tagAttribs.size() > 4 &&
                            tagAttribs.at(1) == "version" &&
                            tagAttribs.at(3) == "encoding")
                    {
                        element.addAttrib("version", tagAttribs.at(2));
                        element.addAttrib("encoding", tagAttribs.at(4));
                    }
                    else
                    {
                        throw QXMLDocumentParseException("XML header parse error");
                    }
                    _root = new QXMLElement(element);
                    currentElement = _root;
                }
                else if (name.at(0) == '/')
                {
                    element.setType(QXMLElement::Type::CLOSER);
                    element.setName(name.mid(1, name.size() - 1));
                    currentElement = currentElement->getParent();
                }
                else
                {
                    element.setType(QXMLElement::Type::DEFAULT);
                    element.setName(tagAttribs.at(0));
                    int size = tagAttribs.size();
                    if (size > 1 && size % 2 == 1)
                        for (int i = 1; i < size; i += 2)
                            element.addAttrib(tagAttribs.at(i), tagAttribs.at(i + 1));
                    auto* elementPtr = new QXMLElement(element);
                    currentElement->addChild(elementPtr);
                    currentElement = elementPtr;
                }
                //TODO Throw parse exception
                if (!_root)
                    return;
            }
        }
        else
        {
            currentElement->setData(item);
        }
    }
}

QXMLDocument::~QXMLDocument()
{

}
QString QXMLDocument::getName() const
{
    return _name;
}
