#include <QCoreApplication>
#include <QTextStream>
#include <QtCore/QFile>

#include <gtest/gtest.h>
#include <iostream>

#include <res.h>

#include <qt_fiction_book.h>

QString getDataFilePath(const char* file)
{
    QString path(DATA_PATH);
    path += file;
    return path;
}

QFictionBook loadFictionBook(const char* name)
{
    QFile file(getDataFilePath(name));
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return QFictionBook();
    }

    QTextStream ts(&file);
    QString bookSource;
    while (!ts.atEnd())
        bookSource += ts.readLine();
    file.close();

    return QFictionBook(QXMLDocument(name, bookSource));
}

TEST(FictionBook2Test, BookParse)
{
    auto book = loadFictionBook("fb2_1.fb2");
    ASSERT_NE(book.getName(), "null");
    ASSERT_STRCASEEQ(
            book.getName().toStdString().c_str(),
            "Волчок и горсть песка");
    ASSERT_STRCASEEQ(
            book.getAuthor().toStdString().c_str(),
            "Алексей Калугин");
    ASSERT_STRCASEEQ(book.getAnnotation().toStdString().c_str(),
            "Великий повелитель Подлунной Гудри-хан достиг всего"
            ", чего хотел. Но страх смерти не дает ему покоя. Пы"
            "таясь узнать дату своей кончины, он посылает за ста"
            "риком с базарной площади своей столицы, который по "
            "горсти песка и волчку может предсказать будущее…\n");
}

// ENCODING FAIL

//TEST(FictionBook1Test, BookParse)
//{
//    auto book = loadFictionBook(getDataFilePath("fb2_2.fb2"));
//    ASSERT_NE(book.getName(), "null");
//    ASSERT_STRCASEEQ(
//            book.getName().toStdString().c_str(),
//            "Понимание");
//    ASSERT_STRCASEEQ(
//            book.getAuthor().toStdString().c_str(),
//            "Тед Чан");
//    ASSERT_STRCASEEQ(book.getAnnotation().toStdString().c_str(),
//            "Рассказ «Понимание», впервые опубликованный в «Asim"
//            "ov's», посвящен глобальным проблемам - сверхразуму "
//            "и Интернету как продолжению человеческого ума. Его "
//            "очевидный литератур¬ный первоисточник - произведени"
//            "е Дэниэла Киза «Цветы для Элджернона» («Flowers for"
//            " Algernon»). Можно подумать, что Чан, прочитав текс"
//            "т, задался вопросом: «А что стало бы с главным геро"
//            "ем, если бы он все умнел и умнел и этот процесс был"
//            " бесконечным?» Ответ - появление сверхразумного гер"
//            "оя. Интересно сравнить трактовку аналогичной пробле"
//            "мы у Грега Игана в рассказе «Причины для счастья»: "
//            "оба писателя изобра¬жают далекое будущее, когда леч"
//            "ение неврологических заболе¬ваний приводит к непред"
//            "сказуемым последствиям.\n");
//}

int main(int argc, char* argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}